<?php

session_start();

  if ($_SESSION['chek'] == true) {

?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title></title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/style_1.css">
</head>

<body>
  <div class="tab-group">
    <a href="/test">Back</a>
  </div>


  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">



        <div class="form">
            <ul class="tab-group">
              <li class="tab active"><a href="#signup">Couriers timetable</a></li>
              <li class="tab"><a href="#login">Page manager</a></li>
            </ul>
            <div class="tab-content">
              <div id="signup">
                <h1>Couriers timetable</h1>

                <form id='courier'>

                  <div class="top-row">
                    <div class="field-wrap">
                      <input type="date" id="start" name="startD" value="2018-07-22" min="2018-01-01" max="2018-12-31">
                    </div>

                    <div class="field-wrap">
                      <input type="time" id="appt" name="startT" min="00:00" max="23:59" required>
                    </div>
                  </div>

                  <div class="top-row">
                    <div class="field-wrap">
                      <input type="date" id="start" name="endD" value="2018-07-22" min="2018-01-01" max="2018-12-31">
                    </div>

                    <div class="field-wrap">
                      <input type="time" id="appt" name="endT" min="00:00" max="23:59" required>
                    </div>
                  </div>



                <div class="field-wrap">
                  <label>
                    Select courier<span class="req">*</span>
                  </label>
                  <input type="text" name = 'name'>
                </div>

                <div class="field-wrap">
                  <label>
                    Destination point<span class="req">*</span>
                  </label>
                  <input type="text" name = 'destination'>
                </div>



                <button class="button button-block"/>add courier</button>
              </form>
              </div>


              <div id="login">
                <h1>Page manager</h1>

                <form id='page' >

                  <div class="field-wrap">
                    <label>
                      Title<span class="req">*</span>
                    </label>
                    <input type="text" name = 'title'>
                  </div>

                  <div class="field-wrap">
                    <label>
                      Meta Description<span class="req">*</span>
                    </label>
                    <input type="text" name = 'meta'>
                  </div>

                  <div class="field-wrap">
                    <textarea type="text" name = 'content'></textarea>
                  </div>

                  <button class="button button-block"/>add page</button>
                </form>

              </div>
            </div><!-- tab-content -->
      </div>


      </div>
    </div>
  </div>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="../js/index.js"></script>
  <script  src="../js/js.js"></script>

  </body>

  </html>
<?php
  }else {
    $home_url = 'http://' . $_SERVER['HTTP_HOST'] . '/test';
    header('Location: ' . $home_url);
  }
//var_dup($_SESSION);
?>
